import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { LoginPage } from '../../pages/login/login';
// import { ServicesPage } from '../../pages/services/services';
import { LoadingController, AlertController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ToastController } from 'ionic-angular';
// import { CustomValidators } from 'ng2-validation';
import { FormBuilder, Validators } from '@angular/forms';
import { MyOrdersPage } from '../../pages/my-orders/my-orders';

@Component({
	selector: 'page-service-detail',
	templateUrl: 'service-detail.html',
})
export class ServiceDetailPage {
	loading: any;
	createSuccess = false;
	userData : any;
	data : { token:"",service_details:string,service_id:"",quantity:"",link:"",price:""};
	serviceDetail : any;
	public addOrderForm;
	validation_messages : any;
	rate:any;

	constructor(public navCtrl: NavController, public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public alertCtrl: AlertController,private auth: AuthServiceProvider,
		public toastCtrl: ToastController, public formBuilder: FormBuilder) {
		this.serviceDetail = this.navParams['data'];
		console.log(this.serviceDetail);
		if(this.serviceDetail.admin_rate != 0){
			this.rate = this.serviceDetail.admin_rate;
		}
		else{
			this.rate = this.serviceDetail.rate;
		}
		this.userData = JSON.parse(localStorage.getItem('user_data'));
		if(this.userData != null){
			//this.navCtrl.setRoot(LoginPage);
		}	
		else {
			this.navCtrl.setRoot(LoginPage);
		}

		this.addOrderForm = formBuilder.group({
			link: ['', Validators.compose([Validators.minLength(2), Validators.required])],
			quantity: ['', Validators.compose([Validators.required])]
			//phone: ['', Validators.compose([Validators.minLength(6), Validators.required])]
		})

		this.validation_messages = {
			'link': [
				{ type: 'required', message: 'URL is required.' },
				{ type: 'minlength', message: 'URL must be at least 2 characters long.' }
			],
			'quantity': [
				{ type: 'required', message: 'Quantity is required.' }
				//{ type: 'minlength', message: 'Last Name must be at least 2 characters long.' }
			]
		}
	}

	ionViewDidLoad() {
		//this.showLoading();
	}

	logout(){
		localStorage.removeItem('user_data');
		this.navCtrl.setRoot(HomePage);
	}

	orderNow(){
		this.showLoading();
		//console.log(this.addOrderForm.value);
		if(this.userData['totalBalance'] > (this.addOrderForm.value.quantity*this.serviceDetail.rate)) 
		{
			if(this.addOrderForm.valid){
				
				this.data = {token : this.userData['loginToken'].token,
							service_details : JSON.stringify(this.serviceDetail),
							service_id : this.serviceDetail.service,
							quantity: this.addOrderForm.value.quantity,
							link: this.addOrderForm.value.link,
							price: this.rate};
				console.log(this.data);
		  		this.auth.requestPost('saveOrder',this.data).then(res=>
		  		{	
		  			if (res['status'] == "1") 
		            {
		            	this.loading.dismiss();
		                this.presentToast(res['message']);
		                this.navCtrl.setRoot(MyOrdersPage);
		            }
		            else 
		            {
		            	this.loading.dismiss();
		                this.showPopup("Error", "Problem saving details.");
		            }
		        },
		        error => 
		        {
		        	this.loading.dismiss();
		            this.presentToast("Error");
		        });
		  	}
		  	else{
		  		this.presentToast("Invalid form data");
		  	}
	  	}
	  	else {
	  		this.loading.dismiss();
	  		this.presentToast("You don't have enough balance for buy this service.")
	  	}
	}

	showPopup(title, text) 
	{
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: text,
			buttons: [
			{
				text: 'OK',
				handler: data => 
				{
					if (this.createSuccess) 
					{
						this.navCtrl.popToRoot();
					}
				}
			}
			]
		});
		alert.present();
	}

	showLoading() 
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
           // dismissOnPageChange: true,
            duration: 1000
        });
        this.loading.present();
    }

    presentToast(message) {
          let toast = this.toastCtrl.create({
            message: message,
            duration: 4000,
            position: 'bottom'
          });

          toast.present();
    }

}
