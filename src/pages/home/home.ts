import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController,MenuController } from 'ionic-angular';

import { LoginPage } from "../../pages/login/login";
import { SignupPage } from "../../pages/signup/signup";
import { UserHomePage } from '../../pages/user-home/user-home';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage 
{
    loading: any;
    createSuccess = false;
    tabBarElement: any;
    constructor(public menu: MenuController,public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams) 
    {
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        if(localStorage.getItem('user_data') != null){
            this.navCtrl.setRoot(UserHomePage);
        }
    }

    ionViewDidLoad() 
    {
        this.menu.enable(false);
    }
    ionViewWillEnter(){
        if(this.tabBarElement){
            this.tabBarElement.style.display = 'none';
        }
    }
    
    ionViewWillLeave(){
        if(this.tabBarElement){
            this.tabBarElement.style.display = 'flex';
        }
    }

    loginPage()
    {
        this.showLoading();
        this.navCtrl.push(LoginPage);
    }

    signupPage()
    {
        this.showLoading();
        this.navCtrl.push(SignupPage);
    }

    showLoading() 
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: true
        });
        this.loading.present();
    }
}
