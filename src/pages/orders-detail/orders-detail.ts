import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { LoadingController, AlertController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { LoginPage } from '../../pages/login/login';

@Component({
	selector: 'page-orders-detail',
	templateUrl: 'orders-detail.html',
})
export class OrdersDetailPage {

	orderDetails : any;
	loading: any;
	createSuccess = false;
	userData : any;
	data : { token:"" , order_id:""};
	orderData : any;
	status: "";
	charge: "";
	currency: "";
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public alertCtrl: AlertController,private auth: AuthServiceProvider) {
		this.orderDetails = this.navParams["data"];
		
		this.userData = JSON.parse(localStorage.getItem('user_data'));
		if(this.userData != null){
			//this.navCtrl.setRoot(LoginPage);
		}	
		else {
			this.navCtrl.setRoot(LoginPage);
		}

		this.getOrderStatus();
	}

	ionViewDidLoad() {
		this.showLoading();
	}

	logout(){
		localStorage.removeItem('user_data');
		this.navCtrl.setRoot(HomePage);
	}

	getOrderStatus(){
		this.data = {token : this.userData['loginToken'].token,order_id:this.orderDetails.order_id};
			
  		this.auth.requestPost('getStatusByOrderId',this.data).then(res=>
  		{	
  			if (res['status'] == "1") 
            {
            	this.orderData = res['data'];
            	this.status = this.orderData['status'];
            	this.charge = this.orderData['charge'];
            	this.currency = this.orderData['currency'];
            }
            else 
            {
                this.showPopup("Error", res['message']);
            }
        },
        error => 
        {
            this.showPopup("Error", error);
        });
	}

	showPopup(title, text) 
	{
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: text,
			buttons: [
			{
				text: 'OK',
				handler: data => 
				{
					if (this.createSuccess) 
					{
						this.navCtrl.popToRoot();
					}
				}
			}
			]
		});
		alert.present();
	}

	showLoading() 
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
           	//dismissOnPageChange: true,
            duration: 1000
        });
        this.loading.present();
    }

}
