import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ServicesPage } from '../../pages/services/services';
import { UserSupportPage } from '../../pages/user-support/user-support';
import { MyOrdersPage } from '../../pages/my-orders/my-orders';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
	//tab1Root = MyOrdersPage;
	tab2Root = ServicesPage;
	tab3Root = UserSupportPage;
	userData : any;
	mySelectedIndex: number;
	tab1Root: any;

	constructor(public navCtrl: NavController, public navParams: NavParams) {
		this.tab1Root = "";
		//console.log(this.tab1Root);
		//console.log(navParams.data.tabIndex);
		this.mySelectedIndex = navParams.data.tabIndex || 0;
		this.tab1Root = navParams.data.tab1Component || MyOrdersPage;
	}

	ionViewDidLoad() {
		//console.log('ionViewDidLoad TabsPage');
	}

}
