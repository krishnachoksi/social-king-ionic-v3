import { Component } from '@angular/core';
import { CustomValidators } from 'ng2-validation';
import { LoginPage } from '../../pages/login/login';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { NavController, NavParams, LoadingController, Loading, AlertController, MenuController } from 'ionic-angular';
import { UserHomePage } from '../../pages/user-home/user-home';
import { ToastController } from 'ionic-angular';

@Component({
	selector: 'page-forgot-password',
	templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage 
{
	public forgotPasswordForm;
    loading: Loading;
    createSuccess = false;
    tabBarElement: any;
	constructor(public navCtrl: NavController, public navParams: NavParams
		, public formBuilder: FormBuilder,
		public alertCtrl: AlertController, public loadingCtrl: LoadingController,
		private auth: AuthServiceProvider,private toastCtrl: ToastController,
         public menu: MenuController) 
	{
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
		if(localStorage.getItem('user_data') != null){
            this.navCtrl.setRoot(UserHomePage);
        }
        
        this.forgotPasswordForm = formBuilder.group({
            forgot_email: ['krishnachoksi1@gmail.com', Validators.compose([Validators.required, CustomValidators.email])]
        });

	}

	popPage()
    {
        this.showLoading();
        this.navCtrl.pop();
    }
    
	ionViewDidLoad() 
    {
        this.menu.enable(false);
    }
    
    ionViewWillEnter(){
        if(this.tabBarElement){
            this.tabBarElement.style.display = 'none';
        }
    }
    
    ionViewWillLeave(){
        if(this.tabBarElement){
            this.tabBarElement.style.display = 'flex';
        }
    }

	forgotPassword()
	{
        if (this.forgotPasswordForm.valid) 
        {
            this.showLoading();
            this.auth.requestPost("forgot-password", this.forgotPasswordForm.value).then(res => 
            {
                if (res['status'] == "1") 
                {      
                	this.presentToast(res['message']);
                    this.navCtrl.pop();
                }
                else 
                {
                    this.presentToast('Problem in sending mail');
                    this.loading.dismiss();
                }
            },
            error => 
            {
                this.presentToast("Error in sending mail.");
            });

        } 
        else 
        {
            this.presentToast("Invalid Form details");
        }
	}

	showLoading() 
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: true
        });
        this.loading.present();
    }

    showPopup(title, text) 
	{
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: text,
			buttons: [
			{
				text: 'OK',
				handler: data => 
				{
					if (this.createSuccess) 
					{
						this.navCtrl.popToRoot();
					}
				}
			}
			]
		});
		alert.present();
	}

    presentToast(message) {
          let toast = this.toastCtrl.create({
            message: message,
            duration: 4000,
            position: 'bottom'
          });

          toast.present();
    }
}
