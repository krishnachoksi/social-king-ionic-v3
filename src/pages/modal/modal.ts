import { Component } from '@angular/core';
import { NavController, NavParams,ViewController } from 'ionic-angular';

@Component({
	selector: 'page-modal',
	templateUrl: 'modal.html',
})
export class ModalPage {

	data: any;
	date: any;
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public view:ViewController) {
		this.data = this.navParams.data;
	}

	ionViewDidLoad() {

	}

	close(){
		this.view.dismiss();
	}
}
