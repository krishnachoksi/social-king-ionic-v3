import { Component } from '@angular/core';
import { CustomValidators } from 'ng2-validation';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginPage } from '../../pages/login/login';
import { NavController, LoadingController, AlertController,MenuController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { UserHomePage } from '../../pages/user-home/user-home';
import { ToastController } from 'ionic-angular';

@Component({
	selector: 'page-signup',
	templateUrl: 'signup.html',
})
export class SignupPage 
{
	public signupForm;
	loading: any;
	createSuccess = false;
	validation_messages : any;
	tabBarElement: any;
	constructor(public navCtrl: NavController, public formBuilder: FormBuilder, public loadingCtrl: LoadingController,
		public alertCtrl: AlertController,private auth: AuthServiceProvider,
		private toastCtrl: ToastController,public menu: MenuController) 
	{
		this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
		if(localStorage.getItem('user_data') != null){
			this.navCtrl.setRoot(UserHomePage);
		}
		this.validation_messages = {
			'firstname': [
				{ type: 'required', message: 'First name is required.' },
				{ type: 'minlength', message: 'First name must be at least 2 characters long.' }
			],
			'lastname': [
				{ type: 'required', message: 'Last Name is required.' },
				{ type: 'minlength', message: 'Last Name must be at least 2 characters long.' }
			],
			'email': [
				{ type: 'required', message: 'Email is required.' },
				{ type: 'email', message: 'Please enter valid email.' }
			],
			'password': [
				{ type: 'required', message: 'Password is required.' },
				{ type: 'minlength', message: 'Password must be at least 8 characters long.' }
			],
		}

		this.signupForm = formBuilder.group({
			firstname: ['', Validators.compose([Validators.minLength(2), Validators.required])],
			lastname: ['', Validators.compose([Validators.minLength(2), Validators.required])],
			email: ['', Validators.compose([Validators.required, CustomValidators.email])],
			password: ['', Validators.compose([Validators.minLength(8), Validators.required])]
			//phone: ['', Validators.compose([Validators.minLength(6), Validators.required])]
		})
	}
	popPage()
	{
		this.showLoading();
		this.navCtrl.pop();
	}

	ionViewDidLoad() {
		this.menu.enable(false);
	}
	
	ionViewWillEnter(){
        if(this.tabBarElement){
            this.tabBarElement.style.display = 'none';
        }
    }
    
    ionViewWillLeave(){
        if(this.tabBarElement){
            this.tabBarElement.style.display = 'flex';
        }
    }
	goToLogin(): void 
	{
		this.showLoading();
		this.navCtrl.push(LoginPage);
	}
	signupUser() 
	{
		if (this.signupForm.valid) 
		{
			this.showLoading();
			this.signupForm.value.name = this.signupForm.value.firstname + " " + this.signupForm.value.lastname;
			
			this.auth.register(this.signupForm.value).subscribe(success => 
			{
				if (success) 
				{
					this.createSuccess = true;
					this.showPopup("Success", "Account created.");
					this.loading.dismiss();
				} 
				else 
				{
					this.showPopup("Error", "Problem creating account.");
					this.loading.dismiss();
				}
			},
			error => 
			{
				this.showPopup("Error", error);
				this.loading.dismiss();
			});
		} 
		else 
		{
			//if(this.signupForm.value.firstname.Invalid){
				this.presentToast("Please enter valid values.");
			//}
			
	//		this.loading.dismiss();
		}
	}
	showPopup(title, text) 
	{
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: text,
			buttons: [
			{
				text: 'OK',
				handler: data => 
				{
					if (this.createSuccess) 
					{
						this.navCtrl.popToRoot();
					}
				}
			}
			]
		});
		alert.present();
	}

	showLoading() 
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: true
        });
        this.loading.present();
    }

    presentToast(message) {
	  	let toast = this.toastCtrl.create({
		    message: message,
		    duration: 4000,
		    position: 'bottom'
	  	});

	  	toast.present();
	}
}
