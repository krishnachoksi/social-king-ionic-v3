import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoadingController, Loading, AlertController,MenuController } from 'ionic-angular';

import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { UserHomePage } from '../user-home/user-home';
import { LoginPage } from '../login/login';

@Component({
	selector: 'page-user-transaction',
	templateUrl: 'user-transaction.html',
})
export class UserTransactionPage {

	userData : any;
	userTransaction : any;
	transaction : string;
	transactionBy : string;
	totalBalance : any;
	loading:Loading;
	constructor(public navCtrl: NavController, public navParams: NavParams,
				public loadingCtrl: LoadingController,
				public alertCtrl: AlertController,
				//private auth: AuthServiceProvider,
				public menu: MenuController,
				private payPal: PayPal) {

		this.userData = JSON.parse(localStorage.getItem('user_data'));
		if(localStorage.getItem('user_data') == null){
            this.navCtrl.setRoot(LoginPage);
        }

        this.userTransaction = this.userData['transactionHistory'];
        this.totalBalance = this.userData['totalBalance'];
        
	}

	ionViewDidLoad() {
		this.showLoading();
		this.menu.enable(true);
	}

	home(){
		//localStorage.removeItem('user_data');
		this.navCtrl.setRoot(UserHomePage);
	}

	showLoading() 
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: true
        });
        this.loading.present();
    }

    addBalance(){
    	this.payPal.init({
  			PayPalEnvironmentProduction: 'AcyMyfu-oPYqSV1fjcGxFxiwMut_f9XtlgWyPOJmu8DBz6O9A6gN-6etaC1UCG14hmePG_RKB2UEkBeq',
  			PayPalEnvironmentSandbox: 'AeTS1z35s0FuhCKlDEONzyjReOKZlATOHFWYnoefiNLOdUz-SvlVDolgj1Ig-Ca_-wS7jVBViNxGPEG3'
		}).then(() => {
		  
		// Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
		this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
		    // Only needed if you get an "Internal Service Error" after PayPal login!
		    //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
		})).then(() => {
    	let payment = new PayPalPayment('3.33', 'USD', 'Description', 'sale');
    			this.payPal.renderSinglePaymentUI(payment).then(() => {
    				console.log(payment);
	      // Successfully paid

	      // Example sandbox response
	      //
	      // {
	      //   "client": {
	      //     "environment": "sandbox",
	      //     "product_name": "PayPal iOS SDK",
	      //     "paypal_sdk_version": "2.16.0",
	      //     "platform": "iOS"
	      //   },
	      //   "response_type": "payment",
	      //   "response": {
	      //     "id": "PAY-1AB23456CD789012EF34GHIJ",
	      //     "state": "approved",
	      //     "create_time": "2016-10-03T13:33:33Z",
	      //     "intent": "sale"
	      //   }
	      // }
    			}, () => {
      		// Error or render dialog closed without being successful
    		});
  		}, () => {
    	// Error in configuration
  		});
		}, () => {
  	// Error in initialization, maybe PayPal isn't supported or something else
		});
    }

}
