import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { UserHomePage } from '../../pages/user-home/user-home';
import { LoadingController, AlertController} from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { LoginPage } from '../login/login';

@Component({
	selector: 'page-about-us',
	templateUrl: 'about-us.html',
})
export class AboutUsPage {

	loading: any;
	createSuccess = false;
	userData : any;
	data : { token:any ,key:any};
    text:any;
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public alertCtrl: AlertController,private auth: AuthServiceProvider,private menu: MenuController) {

		this.userData = JSON.parse(localStorage.getItem('user_data'));
		if(this.userData != null){
			//this.navCtrl.setRoot(LoginPage);
			this.getSetting();
		}	
		else {
			this.navCtrl.setRoot(LoginPage);
		}
	}

	ionViewDidLoad() {
		this.showLoading();
		this.menu.enable(true);
	}

	getSetting(){
        this.data = {token : this.userData['loginToken'].token, key:"AboutUsPageText"};
            
        this.auth.requestPost('getSettingByKey',this.data).then(res=>
        {    
            console.log(res);
            if (res['status'] == "1") 
            {    
                this.text = res['data'];
            }
            else 
            {
             //   this.showPopup("Error", res['message']);
            }
        },
        error => 
        {
			//this.showPopup("Error", error);
        });
    }

	home(){
		this.navCtrl.setRoot(UserHomePage);
	}

	showPopup(title, text) 
	{
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: text,
			buttons: [
			{
				text: 'OK',
				handler: data => 
				{
					if (this.createSuccess) 
					{
						this.navCtrl.popToRoot();
					}
				}
			}
			]
		});
		alert.present();
	}

	showLoading() 
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
           	//dismissOnPageChange: true,
            duration: 1000
        });
        this.loading.present();
    }

}
