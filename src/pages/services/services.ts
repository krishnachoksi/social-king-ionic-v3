import { Component } from '@angular/core';
import { CustomValidators } from 'ng2-validation';
import { NavController, NavParams } from 'ionic-angular';
import { LoadingController, AlertController, MenuController,ToastController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ServiceDetailPage } from '../../pages/service-detail/service-detail';
import { UserHomePage } from '../user-home/user-home';
import { LoginPage } from '../login/login';
import { MyOrdersPage } from '../my-orders/my-orders';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
	selector: 'page-services',
	templateUrl: 'services.html',
})
export class ServicesPage {
	loading: any;
	createSuccess = false;
	userData : any;
	data : { token:""};
	serviceData : any;
	orderData : any;
	public addOrderForm;
	formdata : { token:"",description:"",service_details:string,service_id:"",quantity:"",link:"",price:"",category:"",charge:any};
	serviceDetail : any;
	rate: any;
	textShow = true;
	description : any;
	categories = [];
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public alertCtrl: AlertController,private auth: AuthServiceProvider,
		public menu:MenuController,public formBuilder: FormBuilder,
		public toastCtrl: ToastController) {
		this.userData = JSON.parse(localStorage.getItem('user_data'));
		if(this.userData != null){
			//this.navCtrl.setRoot(LoginPage);
			this.getServices();
		}	
		else {
			this.navCtrl.setRoot(LoginPage);
		}
		//this.showLoading();
		this.addOrderForm = formBuilder.group({
			category: ['', Validators.compose([Validators.required])],
			service_id: ['', Validators.compose([Validators.required])],
			description: [''],
			link: ['', Validators.compose([Validators.required,Validators.pattern("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?")])],
			quantity: ['100', Validators.compose([Validators.required,Validators.min(100), CustomValidators.digits])],
			price: ['']
		})
	}

	ionViewDidLoad() {
		this.showLoading();
		this.menu.enable(true);
	}

	home(){
		//localStorage.removeItem('user_data');
		this.navCtrl.setRoot(UserHomePage);
	}

	getServices(){
		this.showLoading();
		this.data = {token : this.userData['loginToken'].token};
  		this.auth.requestPost('getServices',this.data).then(res=>
  		{
  			setTimeout(()=>{
  				this.loading.dismiss();
  			},1500);

  			if (res['status'] == "1") 
            {
                this.serviceData = res['data']['services'];
                this.categories = [];
                for(let i=0;i<this.serviceData.length;i++) {
                	if(this.categories.indexOf(this.serviceData[i]['category'])==-1) {
                		this.categories.push(this.serviceData[i]['category']);
                	}
                }
            }
            else 
            {
                this.showPopup("Error", "Problem saving details.");
            }
        },
        error => 
        {
            this.showPopup("error", error);
        });
	}

	orderNow(){
		console.log(this.addOrderForm.value);
		if(this.addOrderForm.value.service_id != ""){
			for(let i=0;i<this.serviceData.length;i++){
				if(this.serviceData[i].service == this.addOrderForm.value.service_id){
					this.serviceDetail = this.serviceData[i];
				}
			}
			console.log(this.serviceDetail);

			if(this.serviceDetail.admin_rate != 0){
				this.rate = this.serviceDetail.admin_rate;
			}
			else{
				this.rate = this.serviceDetail.rate;
			}
		}
		
		//console.log(this.serviceData);
		if(this.addOrderForm.valid){
			
			if(this.userData['totalBalance'] > (this.addOrderForm.value.quantity*this.rate)) 
			{
				this.showLoading();
				this.addOrderForm.controls.price.setValue((this.addOrderForm.value.quantity*this.rate));
				this.formdata = {token : this.userData['loginToken'].token,
							description: this.addOrderForm.value.description,
							service_details : JSON.stringify(this.serviceDetail),
							service_id : this.serviceDetail.service,
							quantity: this.addOrderForm.value.quantity,
							link: this.addOrderForm.value.link,
							price: this.rate,
							category: this.addOrderForm.value.category,
							charge: (this.addOrderForm.value.quantity*this.rate)};
				//console.log(this.formdata);
				this.auth.requestPost('saveOrder',this.formdata).then(res=>
				{	
					if (res['status'] == "1") 
					{
						//console.log(res['data']);
						localStorage.setItem('user_data',JSON.stringify(res['data']));
						this.loading.dismiss();
						this.presentToast(res['message']);
						this.navCtrl.setRoot(MyOrdersPage);
					}
					else 
					{
						this.loading.dismiss();
						this.showPopup("Error", "Problem saving details.");
					}
				},
				error => 
				{
					this.loading.dismiss();
					this.presentToast("Error");
				});
			}
			else{
				this.loading.dismiss();
				this.presentToast("You don't have enough balance for buy this service.");
			}
		}
		else {
			this.presentToast("Please enter valid form data");
		}
	}

	addRate(event){
		if(this.addOrderForm.value.service_id != ''){
			for(let i=0;i<this.serviceData.length;i++){
				if(this.serviceData[i].service == this.addOrderForm.value.service_id){
					this.serviceDetail = this.serviceData[i];
				}
			}
			if(this.serviceDetail.admin_rate != 0){
				this.rate = this.serviceDetail.admin_rate;
			}
			else{
				this.rate = this.serviceDetail.rate;
			}
			this.addOrderForm.controls.price.setValue("$"+(this.rate*this.addOrderForm.value.quantity)/1000);
		}
		else{
			this.presentToast("Please select Service first");
		}
		
	}

	change(event){
		if(event.target.value != ''){
			for(let i=0;i<this.serviceData.length;i++){
				if(this.serviceData[i].service == event.target.value){
					this.serviceDetail = this.serviceData[i];
				}
			}
			console.log(this.serviceDetail);
			if(this.serviceDetail.admin_rate != 0){
				this.rate = this.serviceDetail.admin_rate;
			}
			else{
				this.rate = this.serviceDetail.rate;
			}

			if(this.serviceDetail.description != null){
				this.description = this.serviceDetail.description;
				this.addOrderForm.controls.description.setValue(this.description);
				this.textShow = true;
			}
			else{
				this.textShow = false;
			}
			this.addOrderForm.controls.price.setValue("$ "+((this.rate*this.addOrderForm.value.quantity)/1000));	
		}
		else{
			this.presentToast("Please select Service first");
		}
	}
	showPopup(title, text) 
	{
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: text,
			buttons: [
			{
				text: 'OK',
				handler: data => 
				{
					if (this.createSuccess) 
					{
						this.navCtrl.popToRoot();
					}
				}
			}
			]
		});
		alert.present();
	}

	showLoading() 
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
           	dismissOnPageChange: true,
            duration: 1500
        });
        this.loading.present();
    }

    showDetails(data){
    	this.showLoading();
    	this.navCtrl.push(ServiceDetailPage,data);
    }
    
    // ionViewWillEnter(){
    //     this.showLoading();
    // }

    getService(ev: any){
    
    	// set val to the value of the searchbar
	    let val = ev.target.value;

	    // if the value is an empty string don't filter the items
	    if (val && val.trim() != '') {
	      this.serviceData = this.serviceData.filter((item) => {
	        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
	      })
	    }
	    else{
	    	this.getServices();
	    }
    }
    
    onCancel(ev: any){
    	this.getServices();
	}
	
	presentToast(message) {
		let toast = this.toastCtrl.create({
		  message: message,
		  duration: 4000,
		  position: 'bottom'
		});

		toast.present();
  	}

}
