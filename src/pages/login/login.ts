import { Component } from '@angular/core';
import { CustomValidators } from 'ng2-validation';
import { FormBuilder, Validators } from '@angular/forms';
import { UserHomePage } from '../../pages/user-home/user-home';
import { SignupPage } from '../../pages/signup/signup';
import { ForgotPasswordPage } from '../../pages/forgot-password/forgot-password';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { NavController, NavParams, LoadingController, Loading, AlertController, MenuController, Events } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
})
export class LoginPage 
{
	public loginForm;
    loading: Loading;
    createSuccess = false;
    validation_messages : any;
    tabBarElement: any;
	constructor(public navCtrl: NavController, public navParams: NavParams
		, public formBuilder: FormBuilder,
		public alertCtrl: AlertController, public loadingCtrl: LoadingController,
        private auth: AuthServiceProvider,private toastCtrl: ToastController,
        public menu: MenuController,public events: Events) 
	{
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        if(localStorage.getItem('user_data') != null){
            this.navCtrl.setRoot(UserHomePage);
        }
		this.loginForm = formBuilder.group({
            email: ['krishnachoksi1@gmail.com', Validators.compose([Validators.required, CustomValidators.email])],
            password: ['12345678', Validators.compose([Validators.minLength(8), Validators.required])]
        });

        this.validation_messages = {
            'email': [
                { type: 'required', message: 'Email is required.' },
                { type: 'email', message: 'Please enter valid email.' }
            ],
            'password': [
                { type: 'required', message: 'Password is required.' },
                { type: 'minlength', message: 'Password must be at least 6 characters long.' }
            ],
        }
        
	}
	popPage()
    {
        this.showLoading();
        this.navCtrl.pop();
    }
	ionViewDidLoad() {
		this.menu.enable(false);
    }
    ionViewWillEnter(){
        if(this.tabBarElement){
            this.tabBarElement.style.display = 'none';
        }
        this.events.subscribe('user:login_again', (user) => {
            // user and time are the same arguments passed in `events.publish(user, time)`
            console.log('Welcome', user);
        });
    }
    
    ionViewWillLeave(){
        if(this.tabBarElement){
            this.tabBarElement.style.display = 'flex';
        }
    }

	loginUser(): void 
    {

        if (this.loginForm.valid) 
        {
            this.showLoading();
            this.auth.login(this.loginForm.value).subscribe(allowed => 
            {
                if (allowed) 
                {      
                    this.presentToast('Logged in successfully');
                    this.navCtrl.setRoot(UserHomePage);
                }
                else 
                {
                    this.loading.dismiss();
                    this.presentToast("Access Denied");
                }
            },
            error => 
            {
                this.presentToast("Error in login.");
            });

        } 
        else 
        {
            this.presentToast("Invalid Form details");
        }
    }
    showLoading() 
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: true
        });
        this.loading.present();
    }

    presentToast(message) {
          let toast = this.toastCtrl.create({
            message: message,
            duration: 4000,
            position: 'bottom'
          });

          toast.present();
    }
    goToSignup(): void 
    {
        this.showLoading();
        this.navCtrl.push(SignupPage);
    }

    goToForgotPassword(): void 
    {
        this.showLoading();
        this.navCtrl.push(ForgotPasswordPage);
    }

    showPopup(title, text) 
    {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: text,
            buttons: [
            {
                text: 'OK',
                handler: data => 
                {
                    if (this.createSuccess) 
                    {
                        this.navCtrl.popToRoot();
                    }
                }
            }
            ]
        });
        alert.present();
    }

}
