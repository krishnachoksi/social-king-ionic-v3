import { Component} from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { CustomValidators } from 'ng2-validation';
import { FormBuilder, Validators } from '@angular/forms';
import { HomePage } from '../../pages/home/home';
// import { LoginPage } from '../../pages/login/login';
import { ChangePasswordPage } from '../../pages/change-password/change-password';
import { LoadingController, AlertController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { UserHomePage } from '../../pages/user-home/user-home';
// import * as $ from "jquery";
import { ActionSheetController } from 'ionic-angular';
// import { DomSanitizer} from '@angular/platform-browser';
// import { CommonModule } from "@angular/common";
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { ToastController } from 'ionic-angular';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';

@Component({
	selector: 'page-user-profile',
	templateUrl: 'user-profile.html',
})
export class UserProfilePage {

	public profileForm;
	loading: any;
	createSuccess = false;
	userData : any;
	firstname: any;
	lastname: any;
	type : any;
    show = true;
    imageBase64 : any;
    hexColor  : any;
    canvasHeight = "0px";
    canvasWidth = "0px";
    validation_messages:any;
    data:{token:"",profile_pic:""};
    api_url : any;
	constructor(public navCtrl: NavController, 
				public formBuilder: FormBuilder, 
				public loadingCtrl: LoadingController,
				public alertCtrl: AlertController,
				private auth: AuthServiceProvider,
				public actionSheetCtrl: ActionSheetController,
				private camera: Camera,
        		private crop: Crop,
        		private toastCtrl: ToastController,
        		private menu: MenuController,
        		private transfer: FileTransfer) {

		this.userData = JSON.parse(localStorage.getItem('user_data'));
		this.imageBase64 = this.userData['userDetail'].profile_pic;
		this.api_url = this.auth.api_url;

		if(localStorage.getItem('user_data') == null){
            this.navCtrl.setRoot(HomePage);
        }
        if(this.userData != null)
        {
        	this.firstname = this.userData['userDetail'].name.split(" ");
        }

        this.profileForm = formBuilder.group({
			firstname: [this.firstname[0], Validators.compose([Validators.minLength(2), Validators.required])],
			lastname: [this.firstname[1], Validators.compose([Validators.minLength(2), Validators.required])],
			email: [this.userData['userDetail'].email, Validators.compose([Validators.required, CustomValidators.email])]
			//password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
			//phone: ['', Validators.compose([Validators.minLength(6), Validators.required])]
		});

		this.validation_messages = {
			'firstname': [
				{ type: 'required', message: 'First name is required.' },
				{ type: 'minlength', message: 'First name must be at least 2 characters long.' }
			],
			'lastname': [
				{ type: 'required', message: 'Last Name is required.' },
				{ type: 'minlength', message: 'Last Name must be at least 2 characters long.' }
			],
			'email': [
				{ type: 'required', message: 'Email is required.' },
				{ type: 'email', message: 'Please enter valid email.' }
			]
		}
	}

	ionViewDidLoad() {
		this.showLoading();
		this.menu.enable(true);
	}

	/*ionViewWillEnter(){
		this.showLoading();
	}*/

	showLoading() 
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: true,
            duration: 1000
        });
        this.loading.present();
    }

	popPage()
	{
		this.showLoading();
		this.navCtrl.setRoot(UserHomePage);
	}

	editUser(): void{
		if (this.profileForm.valid) 
		{	
			this.showLoading();
			this.profileForm.value.token = this.userData['loginToken'].token;
			this.profileForm.value.name = this.profileForm.value.firstname + " " + this.profileForm.value.lastname;
			this.profileForm.value.id = this.userData['userDetail'].id;

			this.auth.requestPost("updateProfile", this.profileForm.value).then(res => 
            {
                if (res['status'] == "1") 
                {      
                	this.presentToast(res['message']);
                	localStorage.removeItem('user_data');
                	localStorage.setItem('user_data',JSON.stringify(res['data']));
                    //this.navCtrl.setRoot(HomePage);
                    //console.log("login");
                }
                else 
                {
                	this.loading.dismiss();
                    this.presentToast(res['message']);
                }
            },
            error => 
            {
            	this.loading.dismiss();
                this.presentToast("error");
            });
		} 
		else 
		{
			this.presentToast('Invalid form data.');
		}
	}

	presentToast(message) {
          let toast = this.toastCtrl.create({
            message: message,
            duration: 4000,
            position: 'bottom'
          });

          toast.present();
    }

	showPopup(title, text) 
	{
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: text,
			buttons: [
			{
				text: 'OK',
				handler: data => 
				{
					if (this.createSuccess) 
					{
						this.navCtrl.popToRoot();
					}
				}
			}
			]
		});
		alert.present();
	}

	home(){
		//localStorage.removeItem('user_data');
		this.navCtrl.setRoot(UserHomePage);
	}

	goToChangePassword(): void 
	{
		this.showLoading();
		this.navCtrl.push(ChangePasswordPage);
	}

	uploadImage() {
		let actionSheet = this.actionSheetCtrl.create({
	      title: 'Upload Your Profile',
	      buttons: [
	        {
		        text: 'Select From Gallery',
		        //role: 'destructive',
		        handler: () => {
		           this.openGallery()
		        }
	        },{
	         	text: 'Take new photo',
	         	handler: () => {
	            	this.openCamera()
	          	}
	        },{
	          	text: 'Cancel',
	          	role: 'cancel',
	          	handler: () => {
	            	console.log('Cancel clicked');
	          	}
	        }
	      ]
	    });
	    actionSheet.present();
	}

	openCamera(){
		console.log("open camera");
		const options: CameraOptions = 
            {
                quality: 100,
                destinationType: this.camera.DestinationType.FILE_URI,
                encodingType: this.camera.EncodingType.JPEG,
                mediaType: this.camera.MediaType.PICTURE,
                correctOrientation: true
            }
            this.camera.getPicture(options).then((imageData) => 
            {   
            	this.crop.crop(imageData, {quality: 75})
                  .then((
                        newImage) =>
                        {
                            this.show = false;
                            //console.log(imageData);
                            //this.navCtrl.push(DisplayCapturedImagePage,{'image':newImage});   
                            this.imageBase64 = newImage;
                            this.changeProfile(newImage);
                            /*const image = this.imageBase64.split("/");
                            console.log(image);*/
                        },
                (error) => {
                    this.show = true;
                    console.error('Error cropping image', error);
                    this.navCtrl.pop();
                }
              );
            }, 
            (err) => 
            {
                console.log(err);
                this.show = true;
                this.navCtrl.pop();
            });

	}

	openGallery(){
		console.log("open gallery");
		const options: CameraOptions = 
            {
                quality: 70,
                destinationType: this.camera.DestinationType.FILE_URI,
                encodingType: this.camera.EncodingType.JPEG,
                mediaType: this.camera.MediaType.PICTURE,
                correctOrientation: true,
                sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
            }
            this.camera.getPicture(options).then((imageData) => 
            {   

                this.crop.crop(imageData, {quality: 75})
                  .then((
                        newImage) =>
                        {
                            this.show = false;
                            //console.log(imageData);
                            //this.navCtrl.push(DisplayCapturedImagePage,{'image':newImage});   
                            this.imageBase64 = newImage;
                            this.changeProfile(this.imageBase64);
/*                            const image = this.imageBase64.split("/");
                            let imageName = image[image.length-1].split("?")[0];
                            console.log(imageName);*/
                        },
                (error) => {
                    this.show = true;
                    console.error('Error cropping image', error);
                    this.navCtrl.pop();
                }
              );
            }, 
            (err) => 
            {
                console.log(err);
                this.show = true;
                this.navCtrl.pop();
            });
	}

	changeProfile(image){
		this.showLoading();
		this.data = {token:this.userData['loginToken'].token,profile_pic:image};
		const fileTransfer: FileTransferObject = this.transfer.create();
    	var options = {
	     fileKey: "profile_pic",
	     fileName: "filename",
	     chunkedMode: false,
	     mimeType: "multipart/form-data",
	     params : {
	      //"methodName": "updateProfiePicture",
	      "token": this.userData["loginToken"].token
	     }
	    }; 
	    fileTransfer.onProgress((e)=>
	    {
	     //this.prg=(e.lengthComputable) ?  Math.round((e.loaded * 100) / e.total) : -1; 
	     //this.changeDetectorRef.detectChanges();
	    });
	    fileTransfer.upload(image, this.api_url+"updateProfiePicture", options).then((res) => 
	    { 
	     console.log(res['response']);
	     let response = JSON.parse(res['response']);
	     this.presentToast(response.message);
	     localStorage.removeItem('user_data');
         localStorage.setItem('user_data',JSON.stringify(response.data));
	     this.loading.dismiss();
	    },(err)=> {
	    	console.log(err);
	    	this.loading.dismiss();
	    	this.presentToast('Server error.! Please try again later.');
	     //this.viewCtrl.dismiss();
	    });
		
	}
}
