import { Component, ViewChild, ElementRef} from '@angular/core';
// import { CustomValidators } from 'ng2-validation';
import { FormBuilder, Validators } from '@angular/forms';
import { Content } from 'ionic-angular';
// import { LoginPage } from '../../pages/login/login';
import { NavController, LoadingController, AlertController, MenuController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { UserHomePage } from '../user-home/user-home';
import { LoginPage } from '../login/login';
// import { UserHomePage } from '../../pages/user-home/user-home';

@Component({
	selector: 'page-user-support',
	templateUrl: 'user-support.html',
})
export class UserSupportPage {

	public supportForm;
	loading: any;
	createSuccess = false;
	userData : any;
	data : { token:""};
	msgList : any; 
	userDetail: any;
	adminDetail : any;
	image:any;
	msgdata : { token:"", message:""};
	statusData : { token:"", user_id:""};
	@ViewChild(Content) content: Content;
  	@ViewChild('chat_input') messageInput: ElementRef;
  	editorMsg :any; 
	constructor(public navCtrl: NavController, public formBuilder: FormBuilder, public loadingCtrl: LoadingController,
		public alertCtrl: AlertController,private auth: AuthServiceProvider,
		public menu:MenuController) {
		this.userData = JSON.parse(localStorage.getItem('user_data'));
		if(this.userData){
			this.getMessages();
			this.doMessageUnread();
			//this.navCtrl.setRoot(HomePage);
		}
		else{
			this.navCtrl.setRoot(LoginPage);
		}

		this.supportForm = formBuilder.group({
			message: ['', Validators.compose([Validators.required])]
		})

		
	}

	ionViewDidLoad() {
		this.showLoading();
		this.menu.enable(true);
	}

	showLoading() 
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: true
        });
        this.loading.present();
    }

	private onFocus() {
	if (this.messageInput && this.messageInput.nativeElement) {
	      this.messageInput.nativeElement.focus();
	    }
	}

 	private setTextareaScroll() {
    	const textarea =this.messageInput.nativeElement;
    	textarea.scrollTop = textarea.scrollHeight;
  	}

  	sendMsg() {
    	this.msgdata = {
    		token:this.userData['loginToken'].token,
    		message: this.editorMsg
    	}
    	this.auth.requestPost('sendMessages',this.msgdata).then(res=>
  		{
  			if (res['status'] == "1") 
            {   
     		 	this.pushNewMsg(res['data']);
     		 	this.editorMsg = '';
            }
            else 
            {
                this.showPopup("Error", res['message']);
            }
        },
        error => 
        {
            this.showPopup("error", error);
        });
  	}

  	getMessages(){
  		this.data = {token : this.userData['loginToken'].token};
  		this.auth.requestPost('getMessages',this.data).then(res=>
  		{
  			if (res['status'] == "1") 
            {      
            	this.msgList = res['data']['message'];
            	this.userDetail = res['data']['userDetail'];
            	this.adminDetail = res['data']['adminDetail'];
     		 	this.scrollToBottom();
                //console.log("login");
            }
            else 
            {
                this.showPopup("Error", "Problem saving details.");
            }
        },
        error => 
        {
            this.showPopup("error", error);
        });
  	}

  	showPopup(title, text) 
	{
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: text,
			buttons: [
			{
				text: 'OK',
				handler: data => 
				{
					if (this.createSuccess) 
					{
						this.navCtrl.popToRoot();
					}
				}
			}
			]
		});
		alert.present();
	}

	home(){
		//localStorage.removeItem('user_data');
		this.navCtrl.setRoot(UserHomePage);
	}

	scrollToBottom() {
	    setTimeout(() => {
	      if (this.content.scrollToBottom) {
	        this.content.scrollToBottom();
	      }
	    }, 400)
  	}


  	pushNewMsg(msg) {
	    this.msgList.push(msg);
	    
	    this.scrollToBottom();
	}

	doMessageUnread(){
		this.statusData = {
			token: this.userData['loginToken'].token,
			user_id: this.userData['userDetail'].id
		};
		this.auth.requestPost('changeMessageStatus',this.statusData).then(res=>
  		{
  			if(res['status'] == 1){
  				localStorage.setItem('user_data',JSON.stringify(res['data']));
  			}
        },
        error => 
        {
            this.showPopup("error", error);
        });
	}

}
