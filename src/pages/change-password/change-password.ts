import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CustomValidators } from 'ng2-validation';
import { FormBuilder, Validators,FormControl,ValidatorFn, AbstractControl } from '@angular/forms';
import { LoadingController, Loading, AlertController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { LoginPage } from '../../pages/login/login';
import { UserHomePage } from '../../pages/user-home/user-home';
import { ToastController } from 'ionic-angular';

function NotSameAsOld(oldPwd: AbstractControl): ValidatorFn {
  // const hasExclamation = input.value !== this.o_password.value;
  return (control: AbstractControl) => {
    return control.value === oldPwd.value ? {
      NotSameAsOld: {
        valid: false
      }
    } : null;
  };
}

const oldPassword = new FormControl(null, Validators.compose([Validators.required, Validators.minLength(8)]));
const newPassword = new FormControl(null, {
  validators: Validators.compose([Validators.required,
  Validators.minLength(8), NotSameAsOld(oldPassword)])
});
const confirmPassword = new FormControl(null, {
  validators: Validators.compose([CustomValidators.equalTo(newPassword)])
});

@Component({
	selector: 'page-change-password',
	templateUrl: 'change-password.html',
})

export class ChangePasswordPage 
{
	userData: any;
	public changePasswordForm;
	loading: Loading;
    createSuccess = false;
    validation_messages : any;
	constructor(public navCtrl: NavController, 
				public formBuilder: FormBuilder, 
				public loadingCtrl: LoadingController,
				public alertCtrl: AlertController,
				private auth: AuthServiceProvider,
				private toastCtrl: ToastController) 
	{
		if(localStorage.getItem('user_data') == null){
            this.navCtrl.setRoot(LoginPage);
        }
        this.userData = JSON.parse(localStorage.getItem('user_data'));
		this.changePasswordForm = formBuilder.group({
			old_password: oldPassword,
			new_password: newPassword,
			password_confirmation: confirmPassword
		})

		this.validation_messages = {
			'old_password': [
				{ type: 'required', message: 'Old Password is required.' },
				{ type: 'minlength', message: 'Password must be at least 8 characters long.' }
			],
			'new_password': [
				{ type: 'required', message: 'New Password is required.' },
				{ type: 'minlength', message: 'Password must be at least 8 characters long.' },
				{ type: 'NotSameAsOld', message: 'New password can not be same as old password.' }
			],
			'password_confirmation': [
				{ type: 'equalTo', message: 'Please Enter same password' }
			]
		}
	}

	ionViewDidLoad() {
		this.changePasswordForm.reset();
	}

	presentToast(message) {
          let toast = this.toastCtrl.create({
            message: message,
            duration: 4000,
            position: 'bottom'
          });

          toast.present();
    }

	home(){
		this.navCtrl.setRoot(UserHomePage);
	}
	logout(){
		localStorage.removeItem('user_data');
		this.navCtrl.setRoot(LoginPage);
	}

	showLoading() 
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: true
        });
        this.loading.present();
    }

	changePassword() 
	{
		if (this.changePasswordForm.valid) 
		{	
			this.showLoading();
			this.changePasswordForm.value.token = this.userData['loginToken'].token;
	
			this.auth.requestPost("changePassword",this.changePasswordForm.value).then(res => 
			{
				if (res['status'] == 1) 
				{	
					this.changePasswordForm.reset();
					this.createSuccess = true;
					this.presentToast(res['message']);
					this.logout();

				} 
				else 
				{
					this.presentToast(res['message']);
				}
			},
			error => 
			{
				this.presentToast("Error");
				console.log("err");
			});
		} 
		else 
		{
			this.presentToast('Invalid form data.');
		}
	}
	showPopup(title, text) 
	{
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: text,
			buttons: [
			{
				text: 'OK',
				handler: data => 
				{
					if (this.createSuccess) 
					{
						this.logout();
					}
				}
			}
			]
		});
		alert.present();
	}
}
