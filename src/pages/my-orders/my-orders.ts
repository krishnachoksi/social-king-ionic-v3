import { Component } from '@angular/core';
import { NavController, NavParams,MenuController,ModalController } from 'ionic-angular';
import { UserHomePage } from '../../pages/user-home/user-home';
import { LoadingController, AlertController} from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { LoginPage } from '../login/login';
import { ModalPage } from '../../pages/modal/modal';
@Component({
	selector: 'page-my-orders',
	templateUrl: 'my-orders.html',
})
export class MyOrdersPage {
	loading: any;
	createSuccess = false;
	userData : any;
	data : { token:"" };
	serviceDetail : any;
	orderData : any;
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public alertCtrl: AlertController,private auth: AuthServiceProvider,
		private menu: MenuController,
		private modalCtrl:ModalController) {
		this.userData = JSON.parse(localStorage.getItem('user_data'));
		if(this.userData != null){
			//this.navCtrl.setRoot(LoginPage);
			this.getOrders();
		}	
		else {
			this.navCtrl.setRoot(LoginPage);
		}

		
	}

	ionViewDidLoad() {
		this.showLoading();
		this.menu.enable(true);
	}

    orderDetail(data){
    	let modal = this.modalCtrl.create(ModalPage,data);
    	modal.present();
    }
	home(){
		this.navCtrl.setRoot(UserHomePage);
	}

	getOrders(){
		this.data = {token : this.userData['loginToken'].token};
			
  		this.auth.requestPost('getOrderByUserId',this.data).then(res=>
  		{	
  			console.log(res);
  			if (res['status'] == "1") 
            {
            	if(res['data']['orderDetail'].length > 0){
	                this.orderData = res['data']['orderDetail'];
	                for(let i=0;i<this.orderData.length;i++){
	                	this.orderData[i].service_details = JSON.parse(this.orderData[i].service_details);
					}
	            }
	            else{
	            	this.showPopup("Information","You haven't ordered anything yet.")
	            }
            }
            else 
            {
                this.showPopup("Error", res['message']);
            }
        },
        error => 
        {
            this.showPopup("Error", error);
        });
	}


	showPopup(title, text) 
	{
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: text,
			buttons: [
			{
				text: 'OK',
				handler: data => 
				{
					if (this.createSuccess) 
					{
						this.navCtrl.popToRoot();
					}
				}
			}
			]
		});
		alert.present();
	}

	showLoading() 
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
           	//dismissOnPageChange: true,
            duration: 1000
        });
        this.loading.present();
    }

}
