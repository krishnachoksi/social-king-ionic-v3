import { Component,ViewChild } from '@angular/core';
import { NavController, NavParams, Nav } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';
import { UserSupportPage } from '../../pages/user-support/user-support';
import { AddBalancePage } from '../../pages/add-balance/add-balance';
import { LoadingController, Loading, AlertController, MenuController } from 'ionic-angular';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

@Component({
	selector: 'page-user-home',
	templateUrl: 'user-home.html',
})
export class UserHomePage {
	//rootPage = 'TabsPage';
	userData : any;
	loading:Loading;
	data = {token: ""};
	token : any;
	userBalance:any;
	newsData :any;
	@ViewChild(Nav) nav: Nav;
 
  
	constructor(public navCtrl: NavController, public navParams: NavParams,
				public loadingCtrl: LoadingController,
				public alertCtrl: AlertController,public menu:MenuController,
				private payPal: PayPal,private auth: AuthServiceProvider) {
		this.userData = JSON.parse(localStorage.getItem('user_data'));
		this.token = this.userData.loginToken['token'];
		console.log(this.token);
		console.log(this.userData);
		if(this.userData == null){
			this.navCtrl.setRoot(LoginPage);
		}else{
			this.getNewsFeed();
		}	
	}

	ionViewDidLoad() {
		this.menu.enable(true);
		//this.auth.getLogin();
		//console.log(this.userData.messageCount);
	}

	ionViewDidEnter(){
		this.userData = JSON.parse(localStorage.getItem('user_data'));
		if(this.userData != null){
			let balance = this.userData.usedBalance+"";
			this.userData.usedBalance = balance.replace("-", "");
		}
		this.data = {token: this.token};
		setTimeout(() => {
			this.auth.requestPost('getUserBalance',this.data).then(res => 
			{
				console.log(res);
				if (res['status'] == "1") 
				{
					this.userBalance = res['userBalance'];
				} 
				else 
				{
					this.userBalance = 0;
				}
			},
			error => 
			{
				
			});
		}, 10);
		
	}

	logout(){
		localStorage.removeItem('user_data');
		localStorage.removeItem('password');
		this.navCtrl.setRoot(LoginPage);
	}

	supportPage(){
		this.navCtrl.push(UserSupportPage);
	}

	showLoading() 
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: true
        });
        this.loading.present();
	}

	addBalance(){
    	this.navCtrl.setRoot(AddBalancePage);
	}
	
	getNewsFeed(){
		this.data = {token: this.token};
		//setTimeout(() => {
		this.auth.requestPost('getNewsFeed',this.data).then(res => 
		{
			console.log(res);
			if (res['status'] == "1") 
			{
				this.newsData = res['data'];
			} 
			else 
			{
				this.newsData = "";
			}
		},
		error => 
		{
			
		});
		//}, 10);
	}

}
