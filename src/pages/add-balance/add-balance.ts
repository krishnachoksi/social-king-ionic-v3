import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { UserHomePage } from '../../pages/user-home/user-home';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { LoginPage } from '../login/login';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';

@Component({
	selector: 'page-add-balance',
	templateUrl: 'add-balance.html',
})
export class AddBalancePage {
    loading: any;
    createSuccess = false;
    userData : any;
	public balanceForm;
    data : { token:any ,key:any};
    text:any;
    userTransaction : any;
	transaction : string;
	transactionBy : string;
	totalBalance : any;
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public formBuilder: FormBuilder,public loadingCtrl: LoadingController,
        private auth: AuthServiceProvider,public alertCtrl: AlertController,
        private payPal: PayPal) {
		this.balanceForm = formBuilder.group({
            dollar: ['', Validators.compose([Validators.required,Validators.maxLength(8),Validators.pattern(/^[0-9]+(\.[0-9]{1,4})?$/)])]
            //password: ['12345678', Validators.compose([Validators.minLength(6), Validators.required])]
        });    
        this.userData = JSON.parse(localStorage.getItem('user_data'));
        if(this.userData != null){
            //this.navCtrl.setRoot(LoginPage);
            this.getSetting();
        }    
        else {
            this.navCtrl.setRoot(LoginPage);
        }

	}

	ionViewDidLoad() {
	}

	home(){
		this.navCtrl.setRoot(UserHomePage);
	}

    getSetting(){
        this.data = {token : this.userData['loginToken'].token, key:"ProceedPageText"};
            
        this.auth.requestPost('getSettingByKey',this.data).then(res=>
        {    
            //console.log(res);
            if (res['status'] == "1") 
            {    
                this.text = res['data'];
            }
            else 
            {
               // this.showPopup("Error", res['message']);
            }
        },
        error => 
        {
            //this.showPopup("Error", error);
        });
    }

    addBalance(){
        console.log(this.balanceForm.value);
        this.payPal.init({
  			PayPalEnvironmentProduction: 'AcyMyfu-oPYqSV1fjcGxFxiwMut_f9XtlgWyPOJmu8DBz6O9A6gN-6etaC1UCG14hmePG_RKB2UEkBeq',
  			PayPalEnvironmentSandbox: 'AeTS1z35s0FuhCKlDEONzyjReOKZlATOHFWYnoefiNLOdUz-SvlVDolgj1Ig-Ca_-wS7jVBViNxGPEG3'
		}).then(() => {
		  
		// Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
		this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
		    // Only needed if you get an "Internal Service Error" after PayPal login!
		    //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
		})).then(() => {
    	let payment = new PayPalPayment(this.balanceForm.value.dollar, 'USD', 'Adding amount to user account', 'sale');
    			this.payPal.renderSinglePaymentUI(payment).then(() => {
    				console.log(payment);
	      // Successfully paid

	      // Example sandbox response
	      //
	      // {
	      //   "client": {
	      //     "environment": "sandbox",
	      //     "product_name": "PayPal iOS SDK",
	      //     "paypal_sdk_version": "2.16.0",
	      //     "platform": "iOS"
	      //   },
	      //   "response_type": "payment",
	      //   "response": {
	      //     "id": "PAY-1AB23456CD789012EF34GHIJ",
	      //     "state": "approved",
	      //     "create_time": "2016-10-03T13:33:33Z",
	      //     "intent": "sale"
	      //   }
	      // }
    			}, () => {
      		// Error or render dialog closed without being successful
    		});
  		}, () => {
    	// Error in configuration
  		});
		}, () => {
  	// Error in initialization, maybe PayPal isn't supported or something else
		});

    }

    showLoading() 
    {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: true
        });
        this.loading.present();
    }

    showPopup(title, text) 
    {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: text,
            buttons: [
            {
                text: 'OK',
                handler: data => 
                {
                    if (this.createSuccess) 
                    {
                        this.navCtrl.popToRoot();
                    }
                }
            }
            ]
        });
        alert.present();
    }
}
