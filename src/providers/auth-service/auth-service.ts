import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Headers, RequestOptions } from '@angular/http';
import { LoadingController,ToastController,Events } from 'ionic-angular';
import 'rxjs/add/operator/map';

export class User 
{
    name: string;
    email: string;

    constructor(name: string, email: string) 
    {
        this.name = name;
        this.email = email;
    }
}

@Injectable()
export class AuthServiceProvider 
{
    currentUser: User;
    api_url: string;
    loading : any;
    toast : any;
    userData: any;
    password: any;
    data: {email:"",password:""};
    constructor(public http: Http,public loadingCtrl:LoadingController,private toastCtrl: ToastController,
        public events:Events )
    {
        //this.api_url = "http://local.socialking.com/api/";
       //this.api_url = "http://139.59.8.73/social-king/public/api/"
       this.api_url = "http://www.zedny.social/social-king/public/api/";
    }
//http://localhost/drop/Dropbox/api_super_waze/
    public login(credentials) 
    {
        if (credentials.email === null || credentials.password === null) 
        {
            return Observable.throw("Please insert credentials");
        }
        else 
        {
            return Observable.create(observer => 
            {
                this.requestPost("auth/token",credentials)
                .then(res=>
                {
                    if(res['status'] == "1")
                    {
                        var obj = res['data'];
                        localStorage.setItem("user_data",JSON.stringify(obj));
                        localStorage.setItem("password",btoa(credentials.password));
                        this.currentUser = new User(credentials.username, credentials.username);
                        observer.next(true);
                    }
                    else
                    {
                        observer.next(false);
                    }
                    observer.complete();
                });
            });
        }
    }

    requestPost(methodName,data)
    {
        return new Promise(resolve => 
        {
            let headers = new Headers({
                'Content-Type': 'application/x-www-form-urlencoded'
            });

            let options = new RequestOptions({
                headers: headers
            });

            let body = "";

            for (var key in data)
            {
                body += key+"="+data[key]+"&";
            }
            this.http.post(this.api_url+methodName, body, options)
            .subscribe(data => 
            {
                var res = JSON.parse(data['_body']);
                if(res.status == 404){
                    this.getLogin();
                }
                else{
                    resolve(JSON.parse(data['_body']));       
                }
                
            },
            error => 
            {
                resolve({"error":"404"});
            });
        });
    }

    public getLogin()
    {
        this.userData = JSON.parse(localStorage.getItem('user_data'));
        this.password = atob(localStorage.getItem('password'));
        this.data = {email:this.userData['userDetail'].email,password:this.password};
        this.events.publish('user:login_again',this.data);
    }

    public register(credentials) 
    {
        if (credentials.email === null || credentials.password === null) 
        {
            return Observable.throw("Please insert credentials");
        } 
        else 
        {
            // At this point store the credentials to your backend!
            return Observable.create(observer => 
            {

                this.requestPost("register",credentials).then(res=>
                {
                    if(res['status'] == "1")
                    {
                        //localStorage.setItem("user_data",JSON.stringify(obj));
                        //this.currentUser = new User(credentials.username, credentials.username);
                        observer.next(true);
                    }
                    else
                    {
                        observer.next(false);
                    }
                    observer.complete();
                });
                //observer.next(true);
                //observer.complete();
            });
        }
    }

    public getUserInfo() : User 
    {
        return this.currentUser;
    }

    public logout() 
    {
        return Observable.create(observer => 
        {
            localStorage.removeItem("user_data");
            this.currentUser = null;
            observer.next(true);
            observer.complete();
        });
    }
    
    loadingShow(spinner="crescent",content="Please wait...",showBackdrop=true)
    {
        //Possible value of spinner argument => ios,ios-small,bubbles,circles,crescent,dots

        this.loading = this.loadingCtrl.create({
            spinner: spinner,
            content: content,
            showBackdrop: showBackdrop
        });
        this.loading.present();
    }
    
    loadingHide()
    {
        this.loading.dismiss();
    }

    requestGet(method_name) 
    {
        return new Promise(resolve => 
        {
            this.http.get(this.api_url+method_name)
            .map(res => res.json())
            .subscribe(data => 
            {
                console.log(data);
                resolve(data);
            });
        });
    }

    showToast()
    {
        this.toast = this.toastCtrl.create({
            message: 'Retrieving...',
            position: 'middle'
        });

        this.toast.present();
    }
    
    showToastAlert(message)
    {
        this.toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top'
        });

        this.toast.present();
    }

    hideToast()
    {
        this.toast.dismiss();
    }

    filePostData(methodName,data)
    {
      let headers = new Headers({
       'Content-Type': "multipart/form-data"
      });

      let options = new RequestOptions({
       headers: headers
      });
      return this.http.post(this.api_url+methodName, data)
      .map(res => res['_body'])
      .catch((error) => {

       return Observable.throw(error);
      });
    }
}