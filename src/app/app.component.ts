import { Component, ViewChild } from '@angular/core';
import { Nav, Platform,Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from '../pages/tabs/tabs';
import { ServicesPage } from '../pages/services/services';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { MyOrdersPage } from '../pages/my-orders/my-orders';
import { UserProfilePage } from '../pages/user-profile/user-profile';
import { UserSupportPage } from '../pages/user-support/user-support';
import { AddBalancePage } from '../pages/add-balance/add-balance';
import { UserTransactionPage } from '../pages/user-transaction/user-transaction';
import { App } from 'ionic-angular/components/app/app';
import { UserHomePage } from '../pages/user-home/user-home';
import { AboutUsPage } from '../pages/about-us/about-us';
import { LoginPage } from '../pages/login/login';

export interface PageInterface {
  title: string;
  name: string;
  component: any;
  index?: number;
  tabName?: string;
  tabComponent?: any;
}

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  userData: any;
  rootPage:any = TabsPage;
  pages: PageInterface[] = [
    { title: 'My Orders',name:'MyOrdersPage', component: TabsPage,tabComponent: MyOrdersPage, index: 0},
    { title: 'New Order', name:'ServicesPage',component: ServicesPage, index: 1},
    { title: 'Support', name:'UserSupportPage',component: UserSupportPage, index: 2},
    { title: 'Deposit', name:'UserTransactionPage',component: AddBalancePage},  
    { title: 'Profile', name:'UserProfilePage',component: UserProfilePage},
    { title: 'About Us', name:'AboutUsPage',component: AboutUsPage}
  ];
  constructor(public platform: Platform,app: App, public statusBar: StatusBar, public splashScreen: SplashScreen,
    public events:Events, private auth: AuthServiceProvider) {
    this.initializeApp();
    this.userData = JSON.parse(localStorage.getItem("user_data"));

    // used for an example of ngFor and navigation
    // this.pages = [
    //   //{ title: 'Home', component: HomePage },
    //   { title: 'Profile', component: UserProfilePage },
    //   { title: 'Support', component: UserSupportPage },
    //   { title: 'Transaction', component: UserTransactionPage },
    //   { title: 'Services', component: ServicesPage },
    //   { title: 'My Orders', component: MyOrdersPage }
    //   //{ title: 'Login', component: LoginPage } 
    // ];
    this.platform.ready().then(() => {
        this.platform.registerBackButtonAction(() => {
            app.navPop();
        });
    })  
     this.events.subscribe('user:login_again', (user) => {
            this.loginUser(user);
        }); 
  }

  logout(){
    localStorage.removeItem('user_data');
    localStorage.removeItem('password');
    this.nav.setRoot(LoginPage);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page:PageInterface) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    //this.nav.setRoot(page.component);
   this.nav.setRoot(TabsPage, {tab1Component: page.component, tabIndex: page.index});
     // let params = {};
 
     //  // The index is equal to the order of our tabs inside tabs.ts
       
     //   console.log(this.nav.getActiveChildNavs()[0]);
     //   console.log(page.component);
     //  // If tabs page is already ;active just change the tab index
     //  if (this.nav.getActiveChildNavs().length && page.index != undefined) {
     //    //console.log(this.nav.getActiveChildNavs()[0]);
     //    this.nav.getActiveChildNavs()[0].select(page.index);
     //  } else {
     //    // Tabs are not active, so reset the root page 
     //    // In this case: moving to or from SpecialPage
     //    // if (page.index) {
     //      params = { tabIndex: page.index, tab1Component: page.component };
     //    // }
     //    this.nav.setRoot(TabsPage, params);
     //  }
  }

  loginUser(data): void 
    {
      this.auth.login(data).subscribe(allowed => 
      {
          if (allowed) 
          {     
              this.nav.setRoot(UserHomePage);
          }
      },
      error => 
      {
    
      });
    }
}
