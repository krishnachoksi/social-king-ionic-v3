import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ListPage } from '../pages/list/list';
import { PayPal } from '@ionic-native/paypal';

import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { UserHomePage } from '../pages/user-home/user-home';
import { UserProfilePage } from '../pages/user-profile/user-profile';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { ServicesPage } from '../pages/services/services';
import { MyOrdersPage } from '../pages/my-orders/my-orders';
import { AddBalancePage } from '../pages/add-balance/add-balance';
import { ModalPage } from '../pages/modal/modal';
import { OrdersDetailPage } from '../pages/orders-detail/orders-detail';
import { ServiceDetailPage } from '../pages/service-detail/service-detail';
import { UserSupportPage } from '../pages/user-support/user-support';
import { AboutUsPage } from '../pages/about-us/about-us';
import { UserTransactionPage } from '../pages/user-transaction/user-transaction';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { HttpModule } from '@angular/http';
import { Camera } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    SignupPage,
    ForgotPasswordPage,
    UserHomePage,
    UserProfilePage,
    ChangePasswordPage,
    UserSupportPage,
    UserTransactionPage,
    ServicesPage,
    ServiceDetailPage,
    MyOrdersPage,
    OrdersDetailPage,
    TabsPage,
    ModalPage,
    AddBalancePage,
    AboutUsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
		scrollAssist: false,
		autoFocusAssist: false
	}),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    SignupPage,
    ForgotPasswordPage,
    UserHomePage,
    UserProfilePage,
    ChangePasswordPage,
    UserSupportPage,
    UserTransactionPage,
    ServicesPage,
    ServiceDetailPage,
    MyOrdersPage,
    OrdersDetailPage,
    TabsPage,
    ModalPage,
    AddBalancePage,
    AboutUsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    Camera,
    Crop,
    PayPal,
    FileTransfer,
    FileTransferObject,
    File,
  ]
})
export class AppModule {}
